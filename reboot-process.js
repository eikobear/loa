const { basename } = require('path');
const {spawn} = require('child_process');


function rebootProcess() {
  const child = spawn('cmd.exe', ['/c', ...process.argv], {
    detached: true,
    stdio: 'ignore',
  });

  child.unref();
  process.exit();
}

exports.rebootProcess = rebootProcess;
