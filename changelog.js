const fs = require('fs');
const { version, dataVersion } = require('./settings.js');


const changes = {

  '0.9.4': [
    'BIG: improved compatibility with Blitz app! loa will avoid overwriting "Blitz:" runepages and will re-use its own page whenever possible.',
  ],

  '0.9.5': [
    'BIG: loa now syncs summoner spells alongside runes!',
    'loa automatically reboots after updating',
    'loa displays this neat changelog after updating',
  ],
  '0.9.6': [
    'BIG: added a "League of Loa" shortcut to the start bar! Use it to boot league AND loa at the same time!',
  ],
  '0.9.7': [
    'fixed permission issues that prevented shortcuts from being generated',
  ],
}

exports.changelog = () => {
  const oldVersion = dataVersion();
  if (version === oldVersion) return;
  const versions = Object.keys(changes);
  const start = versions.indexOf(oldVersion) + 1;
  const end = versions.indexOf(version) + 1;
  let log = `loa has been updated from ${oldVersion} to ${version}! here's what's new:\n\n`;
  versions.slice(start, end).forEach((v) => {
    log += `${v}\n`;
    log += changes[v].map((c) => `- ${c}`).join('\n');
    log += '\n';
  });
  return log;
};
