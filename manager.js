const fetch = require('node-fetch');
const fs = require('fs');
const { execSync } = require('child_process');
const { version, path, exeName } = require('./settings.js');
const { migrate } = require('./migrate.js');
const { changelog } = require('./changelog.js');
const { rebootProcess } = require('./reboot-process.js');
const makeShortcut = require('./make-shortcut.js');

const versionURL = 'https://gitlab.com/eiko/loa/-/raw/master/settings.json';
const loaDownloadURL = 'https://gitlab.com/eiko/loa/-/raw/master/release/loa-win.exe';

class LoaManager {

  async getNewestVersion() {
    return await fetch(versionURL)
      .then((res) => res.json())
  }

  upToDate(newest, current) {
    const n = newest.split('.').map((d) => Number(d));
    const c = current.split('.').map((d) => Number(d));
    for (let i = 0; i < 3; i++) {
      if (n[i] !== c[i]) return false;
    }

    return true;
  }

  async downloadFile(url, path) {
    const res = await fetch(url);
    const fileStream = fs.createWriteStream(path);
    await new Promise((resolve, reject) => {
      res.body.pipe(fileStream);
      res.body.on("error", reject);
      fileStream.on("finish", resolve);
    });
  }

  async run() {
    console.log(`starting loa v.${version}`);
    if (fs.existsSync(path.root('loa-rollback.exe'))) {
      fs.unlinkSync(path.root('loa-rollback.exe'));
    }
    if (fs.existsSync(path.root('loa-new.exe'))) {
      fs.unlinkSync(path.root('loa-new.exe'));
    }
    const newest = (await this.getNewestVersion()).version;
    if (this.upToDate(newest, version)) {
      const clog = changelog();
      migrate();
      if (clog) console.log(clog);
      makeShortcut();
      return;
    };

    console.log(`loa is out of date! upgrading from ${version} to ${newest}...`);

    if (fs.existsSync(path.root(exeName))) {
      fs.renameSync(path.root(exeName), path.root('loa-rollback.exe'));
      await this.downloadFile(loaDownloadURL, path.root('loa-new.exe'));
      fs.copyFileSync(path.root('loa-new.exe'), path.root(exeName));
      console.log(`upgraded to the newest version! rebooting loa to get you the latest features...`);
      rebootProcess();
    }
    else {
      console.log("no executable detected! update will not be performed\n\n");
    }
  }
}

exports.LoaManager = LoaManager;
