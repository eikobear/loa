const lcu = require("league-connect");
const WebSocket = require("ws");
const https = require("https");
const fs = require("fs");
const { version, path } = require('./settings.js');

class ClientNotRunningError extends Error {
  constructor(message) {
    super(message);
    this.message = "LoL client not running. Did it fail to load?";
    this.name = "ClientNotRunningError";
  }
}

class ConnectionRefusedError extends Error {
  constructor(message) {
    super(message);
    this.message = "Could not connect to LoL client";
    this.name = "ConnectionRefusedError";
  }
}

class Loa {

  async run() {
    this.match = {};
    await this.getRunningClientCreds();
    await this.connectToClient();
    await this.getSummoner();

    this.saveFile = path.data(`${this.summonerId}.json`);
    if (!fs.existsSync(this.saveFile)) {
      fs.writeFileSync(this.saveFile, JSON.stringify({
        top: {}, middle: {}, bottom: {}, utility: {}, jungle: {}, norms: {}, aram: {}
      }));
    }

    this.unsetData = this.getUnsetData();
    this.saveData = JSON.parse(fs.readFileSync(this.saveFile));

    console.log(this.getIntro(this.displayName));

  }

  async getSummoner() {
    const maxTries = -1;
    let tries = 0;
    process.stdout.write('establishing empathic link with summoner..');
    while (maxTries < 0 || tries < maxTries) {
      process.stdout.write('.');
      const response = await lcu.request({
        method: "GET",
        url: `/lol-summoner/v1/current-summoner`,
      }, this.creds);
      const data = await response.json(); 

      if (!data.summonerId) {
        if (data.message !== 'You are not logged in.') {
          console.warn("WARNING: unknown state!", data);
        }
        tries += 1;
        await this.sleep(1000);
        continue;
      }
      else {
        process.stdout.write('\n');
        this.summonerId = data.summonerId;
        this.displayName = data.displayName;
        return;
      }
    }
  }

  async getRunningClientCreds() {
    const maxTries = -1;
    let tries = 0;
    process.stdout.write('waiting for LoL to load..');
    while(maxTries < 0 || tries < maxTries) {
      try {
        process.stdout.write('.');
        this.creds = await lcu.authenticate();
        process.stdout.write('\n');
        return;
      }
      catch (error) {
        tries += 1;
        await this.sleep(1000);
      }
    }

    process.stdout.write('\n');
    throw new ClientNotRunningError();
  }

  wsConnect(url, protocol, options) {
    const ws = new WebSocket(url, protocol, options);
    return new Promise((resolve, reject) => {
      ws.on('open', () => resolve(ws));
      ws.on('error', (error) => {
        ws.close();
        reject(error);
      });
    });
  }

  async connectToClient() {
    let maxTries = -1;
    let tries = 0;
    process.stdout.write('connecting to LoL client..');
    while(maxTries < 0 || tries < maxTries) {
      try {
        process.stdout.write('.');
        const url = `wss://riot:${this.creds.password}@127.0.0.1:${this.creds.port}`;
        this.ws = await this.wsConnect(url, 'wamp', {
          agent: new https.Agent({ ca: this.creds.certificate }),
        });
        this.ws.send(JSON.stringify([5, 'OnJsonApiEvent']))
        this.ws.on('message', (message) => {
          const data = JSON.parse(message).slice(2)[0];
          if (data.uri === '/lol-champ-select/v1/session') {
            this.handleChampUpdate(data.data);
          }

          if (data.uri === '/riotclient/pre-shutdown/begin') {
            this.handleShutdown();
          }
        });
        //this.ws = await lcu.connect(this.creds);
        //this.ws.subscribe('/lol-perks/v1/currentpage', (data, event) => {});
        //this.ws.subscribe('/lol-champ-select/v1/session', (data) => {
        //  this.handleChampUpdate(data);
        //});
        process.stdout.write('\n');
        return;
      }
      catch (error) {
        tries += 1;
        await this.sleep(1000);
      }
    }

    process.stdout.write('\n');
    throw new ConnectionRefusedError();
  }

  async handleShutdown() {
    this.ws.close();
    console.log("ggs!");
    await this.sleep(500);
    console.log("let's play again some time ^-^");
    await this.sleep(500);
    console.log(" ");
    await this.sleep(600);
    console.log(" ");
    await this.sleep(700);
    console.log(" ");
    await this.sleep(800);
    console.log("okay bye!!");
    await this.sleep(300);
    process.exit();
  }

  sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }   

  getIntro(name) {
    return this.pick([
      `hello ${name}! ready to crush some noobs?? >:3c\n`,
      `welcome to the league of ${name} c:<\n`,
      `looks like ${name} is back again for more 0 and 10!\n`,
      `hi ${name}! let's get a win tonight c:\n`,
      `hey ${name}, let's run some extra spicy runes today >:3c\n`,
      `${name} has entered the fray!\n`,
      `hi ${name}! let's keep that loss streak rolling\n`,
      `you know ${name}, winning isn't as important as the runes you pick along the way...\n`,
      `alright ${name}, let's get out there and win some esports!\n`,
      `${name}, let's play for fun tonight c:\n`,
    ]);
  }

  pick(a) {
    const index = Math.floor(a.length * Math.random());
    return a[index];
  }

  getUnsetData() {
    const unsetFunset = {
      runes: {
        current: true,
        name: "UNSET FUNSET",
        primaryStyleId: 8300,
        selectedPerkIds: [
          8360,
          8306,
          8321,
          8352,
          8120,
          8135,
          5008,
          5008,
          5001
        ],
        subStyleId: 8100
      },
      version, 
    };

    const unsetFile = path.data('unset.json');
    if (!fs.existsSync(unsetFile)) {
      fs.writeFileSync(unsetFile, JSON.stringify(unsetFunset, null, 2));
    }

    return JSON.parse(fs.readFileSync(unsetFile));
  }


  async  getChampionName(summonerId, championId) {
    const response = await lcu.request({
      method: "GET",
      url: `lol-champions/v1/inventories/${summonerId}/champions/${championId}`,
    }, this.creds);

    const data = await response.json();
    return data.name;
  }

  async  handleChampUpdate(data) {
    const me = data.myTeam.find((teammate) => teammate.summonerId === this.summonerId);

    if (this.match.inProgress && data.timer.phase === '') {
      if (this.match.starting) {
        console.log('glhf ^ O^\n\n');
      }
      else {
        console.log('oof a dodge :T (saving your runes anyway)');
        await this.saveUserData();
      }

      delete this.match.inProgress;
      delete this.match.role;
      delete this.match.championId;
      delete this.match.championName;
      delete this.match.locked;
      delete this.match.starting;
      return;
    }

    if (!this.match.inProgress) {
      if (data.benchEnabled) {
        this.match.role = 'aram';
        console.log("new game detected: aram");
      }
      else if (me.assignedPosition === '') {
        this.match.role = 'norms';
        console.log("new game detected: blind pick");
      }
      else {
        this.match.role = me.assignedPosition;
        console.log("new game detected: draft pick");
      }
      this.match.inProgress = true;
    }

    if (data.timer.phase === 'GAME_STARTING') {
      this.match.starting = true;
      console.log(`match starting! saving runes for next time...`);
      this.saveUserData();
      return
    }
    
    const championId = me.championId || me.championPickIntent;

    if (this.match.championId !== championId) {
      this.match.championId = championId;
      this.match.championName = await this.getChampionName(this.summonerId, this.match.championId);
      this.handleChampSelect();
    }

    if (!this.match.locked && me.championId) {
      console.log(`locked: ${this.match.championName}`);
      this.match.locked = true;
    }
  }

  sanitizePage(page) {
    return {
      current: true,
      name: page.name,
      primaryStyleId: page.primaryStyleId,
      selectedPerkIds: page.selectedPerkIds,
      subStyleId: page.subStyleId,
    };

  }

  async  handleChampSelect() {
    if (!this.match.championId) return;

    const { role, championId } = this.match;
    let data = this.saveData.champions[championId]?.roles[role] || { ...this.unsetData };
    console.log(`setting runes: ${data.runes.name}`);
    this.setRunePage(data.runes);
    if (data.spells) this.setSpells(data.spells);
  }

  async getCurrentSpells() {
    const response = await lcu.request({
      method: "GET",
      url: `/lol-settings/v2/account/LCUPreferences/custom-spell-selections`,
    }, this.creds);
    const spells = await response.json();
    return spells.data['custom-spell-selections'];
  }

  async setSpells({ spell1, spell2 }) {
    await lcu.request({
      method: "PATCH",
      url: `/lol-champ-select/v1/session/my-selection`,
      body: { spell1Id: spell1, spell2Id: spell2 },
    }, this.creds);
  }

  async  saveUserData() {
    const { role, championName, championId } = this.match;
    const current = await this.getCurrentPage();
    const runes = this.sanitizePage(current);
    const spells = await this.getCurrentSpells();
    runes.name = `[${role[0]}] ${championName}`;
    this.saveData.champions[championId] = this.saveData.champions[championId] || { roles: {}};
    this.saveData.champions[championId].roles[role] = { runes, spells };
    fs.writeFileSync(this.saveFile, JSON.stringify(this.saveData, null, 2));
  }

  async  getCurrentPage() {
    const response = await lcu.request({
      method: "GET",
      url: `/lol-perks/v1/pages`,
    }, this.creds);
    const pages = await response.json();
    return pages.find((page) => page.current);
  }

  async  getLoaPage() {
    const response = await lcu.request({
      method: "GET",
      url: `/lol-perks/v1/pages`,
    }, this.creds);
    const pages = await response.json();
    return pages.find((page) => page.name.startsWith('loa: '));
  }

  async  getUnusedPage() {
    const response = await lcu.request({
      method: "GET",
      url: `/lol-perks/v1/pages`,
    }, this.creds);
    const pages = await response.json();
    return pages.find((page) => !page.name.startsWith('Blitz: ') && page.isEditable);
  }

  async  setRunePage(newPage) {
    let page = await this.getLoaPage();
    if (!page) page = await this.getCurrentPage();
    if (!page || page.name.startsWith('Blitz: ')) page = await this.getUnusedPage();
    if (!page) {
      console.log("ERROR: could not find a suitable runepage to set");
      console.log("Do you have at least 1 editable page not being used by Blitz?");
      console.log("Falling back to legacy behavior- will overwrite current page!");
      page = await this.getCurrentPage();
    }

    newPage = {
      ...newPage,
      name: `loa: ${newPage.name}`,
    };

    await lcu.request({
      method: "PUT",
      url: `/lol-perks/v1/pages/${page.id}`,
      body: newPage,
    }, this.creds);
  }

}

exports.Loa = Loa;
exports.ClientNotRunningError = ClientNotRunningError;
exports.ConnectionRefusedError = ConnectionRefusedError;
