require('dotenv').config()
const fs = require('fs');
const { dirname, basename, join } = require('path');
const { version } = require('./settings.json');

const devmode = !!process.env.DEVELOPMENT_MODE;
const dataDir = 'loa-data';
const versionFile = 'version.json';

const path = {
  data: (...paths) => join(rootDir, dataDir, ...paths),
  root: (...paths) => join(rootDir, ...paths),
  startMenu: (...paths) => join(process.env.APPDATA, 'Microsoft/Windows/Start Menu/Programs', ...paths),
};

const dataVersion = () => {
  if (!fs.existsSync(path.data(versionFile))) return '0.9.3';
  return JSON.parse(fs.readFileSync(path.data(versionFile))).version;
}

const rootDir = devmode? __dirname : dirname(process.argv[0]);

const exeName = devmode? 'development.exe' : basename(process.argv[0]);


module.exports = {
  version,
  devmode,
  dataDir,
  versionFile,
  dataVersion,
  rootDir,
  exeName,
  path,
}

