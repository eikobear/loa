const fs = require('fs');
const { version, path, versionFile, dataVersion } = require('./settings.js');

const uDataRx = /^[0-9]+.json$/;

const prettyJSON = (data) => JSON.stringify(data, null, 2);

const mapUserData = (callback) => {
  fs.readdirSync(path.data()).filter((file) => uDataRx.test(file)).forEach((file) => {
    const data = JSON.parse(fs.readFileSync(path.data(file)));
    const mapped = callback(data);
    if (mapped === undefined) return;
    fs.writeFileSync(path.data(file), prettyJSON(mapped));
  });
};

const mapUnset = (callback) => {
  if (!fs.existsSync(path.data('unset.json'))) return;
  const data = JSON.parse(fs.readFileSync(path.data('unset.json')));
  const mapped = callback(data);
  if (mapped === undefined) return;
  fs.writeFileSync(path.data('unset.json'), prettyJSON(mapped));
};

const updateVersionFile = (version) => {
  fs.writeFileSync(path.data(versionFile), prettyJSON({ version }));
}

const isNewInstall = () => {
  const dirExists = fs.existsSync(path.data());
  const fileExists = fs.readdirSync(path.root()).find((f) => uDataRx.test(f));
  const unsetExists = fs.existsSync(path.root('unset.json'));
  return !(dirExists || fileExists || unsetExists);
}

const ezMigrate = (version) => {
  mapUserData((data) => ({ ...data, version }));
  mapUnset((data) => ({ ...data, version }));
  updateVersionFile(version);
}

const initialMigration = () => {
  // create the new data directory
  if (!fs.existsSync(path.data())){
    fs.mkdirSync(path.data());
  }

  // move all JSON into the new data directory
  fs.readdirSync(path.root()).filter((file) => uDataRx.test(file)).forEach((jsonFile) => {
    fs.renameSync(path.root(jsonFile), path.data(jsonFile));
  });

  // add versions to data files so we know how out of date they are
  fs.readdirSync(path.data()).filter((file) => uDataRx.test(file)).forEach((jsonFile) => {
    const data = JSON.parse(fs.readFileSync(path.data(jsonFile)));
    data.version = '0.9.4';
    fs.writeFileSync(path.data(jsonFile), prettyJSON(data));
  });

  // move "unset.json" specifically
  if (fs.existsSync(path.root('unset.json'))) {
    fs.renameSync(path.root('unset.json'), path.data('unset.json'));
    const data = JSON.parse(fs.readFileSync(path.data('unset.json')));
    data.version = '0.9.4';
    fs.writeFileSync(path.data('unset.json'), prettyJSON(data));
  }

  // add the new version file for tracking the version of the app itself
  updateVersionFile('0.9.4');
}

const migrations = {
  '0.9.4': () => {
    mapUserData((data) => {
      const newData = { version: '0.9.5', champions: {} };
      delete data.version;
      Object.entries(data).forEach(([role, champs]) => {
        Object.entries(champs).forEach(([champ, runePage]) => {
          newData.champions[champ] = newData.champions[champ] || { roles: {}};
          newData.champions[champ].roles[role] = { runes: runePage };
        });
      });
      return newData;
    });

    mapUnset((data) => {
      const newData = {};
      newData.runes = data.unset;
      newData.version = '0.9.5';
      return newData;
    });

    updateVersionFile('0.9.5');
  },
  '0.9.5': () => ezMigrate('0.9.6'),
  '0.9.6': () => ezMigrate('0.9.7'),
}

exports.migrate = () => {
  if (isNewInstall()) {
    console.log("new installation detected! generating user configuration files...");
  }

  if (dataVersion() === '0.9.3') initialMigration();

  while (true) {
    const v = dataVersion();
    if (v === version) return {};
    if (!Object.keys(migrations).includes(v)) return { errors: [`invalid data version (${v}) cannot be migrated to application version (${version})`] };
    migrations[v]();
  }
}

