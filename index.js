const { LoaManager } = require("./manager.js");
const { Loa } = require("./loa.js");
const { version } = require("./settings.js");

if (process.argv[2] === '--version') {
  console.log(version);
  process.exit();
}
const manager = new LoaManager();

manager.run().then(() => {
  const loa = new Loa();

  loa.run().catch((error) => {
    console.log("error!", error.message);
    loa.sleep(5000);
    process.exit();
  });
});
