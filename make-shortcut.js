const { path, exeName, devmode } = require('./settings.js');
const shortcut = require('windows-shortcuts');
const fs = require('fs');
const { spawn } = require("child_process");


//const moveOldLink = () => {
//  if (fs.existsSync(path.startMenu
//

const psMakeShortcut =
`$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("${path.startMenu('League of Loa.lnk')}")
$Shortcut.TargetPath = "${path.root('start-lol-and-loa.bat')}"
$Shortcut.IconLocation = "${process.env.SYSTEMDRIVE}/ProgramData/Riot Games/Metadata/league_of_legends.live/league_of_legends.live.ico"
$Shortcut.Save()`

const batchText = 
`cd "C:/Riot Games/Riot Client"
start RiotClientServices.exe --launch-product=league_of_legends --launch-patchline=live
cd "${path.root()}"
start ${exeName}`;


module.exports = () => {
  if (devmode) return;

  fs.writeFileSync(path.root('start-lol-and-loa.bat'), batchText);

  if (fs.existsSync(path.startMenu('League of Loa.lnk'))) return;

  console.log('no shortcut detected! Adding "League of Loa" to your start menu!');
  
  fs.writeFileSync(path.root('make-shortcut.ps1'), psMakeShortcut);

  const child = spawn("powershell.exe", [path.root('make-shortcut.ps1')]);
  
  setTimeout(() => fs.unlinkSync(path.root('make-shortcut.ps1')), 3000);
};


